<?php
namespace WordCounter;

interface Grabber
{
    // public function apply(array $array);
    public function validateWord(string $word);
}
