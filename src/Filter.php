<?php
namespace WordCounter;

/**
 * Parent Class
 * Common logic for ALL filters
 * May have:
 *  - properties
 *  - body inside methods
 */
class Filter
{
    // public $arr = [];
    // public function __construct(array $arr)
    // {
    //     $this->arr= $arr;
    // }
    // actual filter
    public function apply(Filter $filter, array $input)  // aqui esta ocurriendo abstracción, debido a que todos cumplen este contrato, "da igual" cual filtro sea
    {
        $arr_result = [];
        foreach ($input as $word) {
            if ($filter->validateWord($word)) {
                $arr_result[] = $word;
            }
        }
        return $this->arr = $arr_result;  // "los filtros devuelven un array de palabras"
    }
}
