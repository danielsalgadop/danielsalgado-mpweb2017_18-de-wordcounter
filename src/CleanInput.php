<?php
namespace WordCounter;

class CleanInput
{
    public $text = '';

    public function __construct($text)
    {
        $this->text = $text;
    }
    public function CleanSentence()
    {
        $this->CleanString();
        return $this->ToArray();
    }
    public function CleanString()
    {
        $this->text = $this->cleanSigns($this->cleanSpaces());
    }
    public function ToArray()
    {
        return $this->splitTextBySpaces();
    }

    private function splitTextBySpaces()
    {
        return explode(' ', $this->text);
    }

    private function cleanSpaces()
    {
        $this->text = preg_replace('/\s+/', ' ', trim($this->text));
    }
    private function cleanSigns()
    {
        $string = preg_replace('/\./', '', $this->text);
        return preg_replace('/,/', '', $string);
    }
}
