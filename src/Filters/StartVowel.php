<?php
namespace WordCounter\Filters;

use WordCounter\Filter;
use WordCounter\Grabber;

class StartVowel extends Filter implements Grabber
{
    public function validateWord(string $word): bool
    {
        return (preg_match("/^[aeiou]/i", $word))?true:false;
    }
}
