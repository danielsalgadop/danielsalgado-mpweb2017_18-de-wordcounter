<?php
namespace WordCounter\Filters;

use WordCounter\Filter;
use WordCounter\Grabber;

class MoreThan2Characters extends Filter implements Grabber
{
    public function validateWord(string $word): bool
    {
        return (preg_match("/^.{3,}$/", $word))?true:false;
    }
}
