<?php

namespace WordCounter\Filters;

use WordCounter\Filter;
use WordCounter\Grabber;

class KeyWords extends Filter implements Grabber
{
    public $arr_words_searched = [];
    public function __construct(array $arr_words_searched)
    {
        $this->arr_words_searched = array_map('strtoupper', $arr_words_searched);
    }
    public function validateWord(string $word): bool
    {
        return (in_array(strtoupper($word), $this->arr_words_searched))?true:false;
    }
}
