<?php
namespace WordCounter;
require('vendor/autoload.php');
$example_text = "Esto es un texto molón que sirve como juego de pruebas para la kata de contar palabrejas. No me hagas

un diseño de gañán ni de hiper-arquitecto. Que te veo, eh.
";
$arr_keywords = ["palabrejas", "gañán", "hiper-arquitecto", "que", "eh"];

// mi idea para el update de sesión 4 es hacer una Factory de Filtros, y creo que hay 2 grandes grupos de Filtros, los que usan regexp y los que no. Quizás hacer un Factory con 2 abrtracciones o un Decorator... we will see
use WordCounter\CleanInput;
use WordCounter\Grabber;
use WordCounter\Filter;
use WordCounter\Filters\TotalWords;
use WordCounter\Filters\StartVowel;
use WordCounter\Filters\MoreThan2Characters;
use WordCounter\Filters\KeyWords;




class WordCounter
{
    public $filters_name = [];
    public $arr_filters = [];
    public $arr = [];
    public function __construct(array $arr_filters,array $input)
    {
        $this->arr = $input;
        $this->arr_filters[] = $arr_filters;
        foreach ($arr_filters as $filter) {
            $this->arr = $filter->apply($filter,$this->arr);
            $this->filters_name[]=(string) get_class($filter);
        }
    }
    public function getResult()
    {
        return $this->arr;
    }
    public function countWords()
    {
        return (int) count($this->arr);
    }
    public function __toString()
    {
        return implode('-', $this->filters_name). '['.$this->countWords()."]\n";
    }
}



// instanciate filters
$totalWordsFilter = new TotalWords;
$startVowelFilter = new StartVowel;
$moreThan2CharactersFilter = new MoreThan2Characters;

// Tarea3
// CleanInput ES un filtro, pensar como integrarlo como tal
$cleanInput = new CleanInput($example_text);
$cleanSentence = $cleanInput->cleanSentence();
$totalWords = new WordCounter([$totalWordsFilter],$cleanSentence);
print $totalWords;


$startVowel = new WordCounter([$startVowelFilter],$cleanSentence);
print $startVowel;

$moreThan2Characters = new WordCounter([$moreThan2CharactersFilter],$cleanSentence);
print $moreThan2Characters;

$keyWords = new WordCounter([new KeyWords ($arr_keywords) ] ,$cleanSentence );
print $keyWords;


// Tarea4
//////////// concatenating 2 Filters !

$compuesto1 = new WordCounter([$moreThan2CharactersFilter, $startVowelFilter],$cleanSentence);
print $compuesto1;

$compuesto2 = new WordCounter([new Keywords ($arr_keywords), $startVowelFilter], $cleanSentence) ;
print $compuesto2;


$compuesto3 = new WordCounter([new Keywords ($arr_keywords), $startVowelFilter, $moreThan2CharactersFilter], $cleanSentence) ;
print $compuesto3;
