Entregable: Word Counter

Este ejercicio se presenta en diferentes enunciados a lo largo de la asignatura y se irá desarrollando de forma incremental. Se trata de contar las palabras que pasen ciertos filtros o ciertos filtros compuestos de otros más simples. Este enunciado tiene dos tareas:



Tarea 1

Deberás diseñar un programa que permita aplicar filtros de palabras a un texto, el funcionamiento debe ser similar al del programa que encontrarás en el siguiente enlace: Word Counter (https://wordcounter.net/). Recuerda que debes aplicar los conceptos de Diseño Eficiente. Deberá printar por pantalla el resultado de llamar a las funcionalidades que se piden para cada entregable, aplicando para cada uno, al menos, todos los conceptos aprendidos hasta esa sesión.



Texto de ejemplo:

Esto es un texto molón que sirve como juego de pruebas para la kata de contar palabrejas. No me hagas

un diseño de gañán ni de hiper-arquitecto. Que te veo, eh.



Recuerda que el proyecto debe estar provisionado con composer. Commits frecuentes con mensajes descriptivos.



El nombre del proyecto será [NombreApellido]-mpweb[año]-de-WordCounter. NombreApellido corresponde al nombre y apellido del alumno y año corresponde al periodo que se cursa el Máster, por ejemplo, al Máster cursado la última mitad del año 2016 y la primera mitad del año 2017 le corresponde año=2016_17.



A entregar:

URL del repositorio Word Counter. Dar al usuario del mentor acceso de lectura.



Tarea 2

Word Counter debe ser capaz de contar las palabras totales del texto de ejemplo.

Palabras totales: 31



Una vez ejecutado el enunciado de esta sesión marca el último commit con el tag [total]. Recuerda subir el tag al repositorio remoto ya que por defecto al hacer push no se suben los tags.

Para saber cómo crear un tag consulta el enlace: Git Basics Tagging.



A entregar:

URL del repositorio Word Counter apuntando al tag correspondientes al ejercicio de esta sesión.



El nombre del fichero txt a entregar debe ser el mismo que el nombre del repositorio.

/////////////////////////////// Update Sesion 3

Word Counter debe ser capaz de contar las palabras totales filtradas del texto de ejemplo.

Los filtros serán (entre paréntesis la solución):

    Palabras que empiecen por vocal (5)
    Palabras de más de dos caracteres (18)
    Palabras clave (6)
        palabrejas, gañán, hiper-arquitecto, que, eh.

Una vez ejecutado el enunciado de esta sesión marca el último commit con el tag [filtros]. Recuerda subir el tag al repositorio remoto ya que por defecto al hacer push no se suben los tags.

/////////////////////////////// Update Sesion 4

Word Counter debe ser capaz de contar las palabras totales filtradas del texto de ejemplo.

Los filtros serán (entre paréntesis la solución):

    Contar solo las palabras que empiecen por vocal y tengan más de 2 caracteres (1)
    Contar solo palabras clave que empiecen por vocal (1)
    Contar solo palabras clave, que empiecen por vocal y tengan más de dos carácteres (0)
        Keywors: palabrejas, gañán, hiper-arquitecto, que, eh.

Puede que te ayude un patrón de diseño.

Una vez ejecutado el enunciado de esta sesión marca el último commit con el tag [filtroscompuestos]. Recuerda subir el tag al repositorio remoto ya que por defecto al hacer push no se suben los tags.

